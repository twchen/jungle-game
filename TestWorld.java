import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import javax.swing.JOptionPane;
import javax.swing.JInternalFrame;

/**
 * Write a description of class TestWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class TestWorld extends GWorld
{
    static {      
        // Initialize the world
        GWorld.setWidth(7);  // 
        GWorld.setHeight(9);
        GWorld.setCellSize(60);
    }
    
    public static int movementDir;
    /**
     * Constructor for objects of class TestWorld.
     * 
     */
    public void initialize() {                 
       initializeLandscape();
       initializeChess();
    }
    
    /**
     * initialize the chess.
     */
    private void initializeChess()
    {
        //add chesses;
        int[] xArray = {6,1,5,2,4,0,6,0};
        int[] yArray = {6,7,7,6,6,8,8,6};
        for(int i=0;i<8;i++){
            if(i==5 || i==6){
                Chess c0 = new JumpableChess(0,i+1);
                Chess c1 = new JumpableChess(1,i+1);
                GWorld.addOneObject(c0,6-xArray[i],8-yArray[i]);
                GWorld.addOneObject(c1,xArray[i],yArray[i]);
            }
            else{
                Chess c0 = new Chess(0,i+1);
                Chess c1 = new Chess(1,i+1);
                GWorld.addOneObject(c0,6-xArray[i],8-yArray[i]);
                GWorld.addOneObject(c1,xArray[i],yArray[i]);
            }
        }
    }
    
    /**
     * to test the movement methods to an empty dirt cell;
     * input an integer for the tested direction(Up is 0,Left is 1,Down is 2,Right is 3);
     * invoke this method,so chess pieces will be added to the proper locations,and then click the "Act" or "Run" button,
     * all chess pieces will move towards the tested direction.
     */
    public void emptyDirtCell(int dir){
        if(dir == 3){
            for(int i=0;i<8;i++){
                GWorld.addOneObject(new Chess(0,i+1),0,i);
                GWorld.addOneObject(new Chess(1,i+1),3,i);
            }
        }
        else if(dir == 1){
            for(int i=0;i<8;i++){
                GWorld.addOneObject(new Chess(0,i+1),3,i);
                GWorld.addOneObject(new Chess(1,i+1),6,i);
            }
        }
        else if(dir == 2){
            for(int i=0;i<8;i++){
                GWorld.addOneObject(new Chess(0,i+1),i%4,(i/4)*2);
                GWorld.addOneObject(new Chess(1,i+1),i%4,(i/4)*2+4);
            }
        }
        else {
            for(int i=0;i<8;i++){
                GWorld.addOneObject(new Chess(0,i+1),i%4,(i/4+1)*2);
                GWorld.addOneObject(new Chess(1,i+1),i%4,(i/4+1)*2+4);
            }
        }
        movementDir = dir+1;
    }
    
    /**
     * to test the movement methods to water;
     * similar to the emptyDirtCell() method,but add some water objects.
     */
    public void water(int dir){
        if(dir == 3){
            for(int i=0;i<8;i++){
                GWorld.addOneObject(new Water(),1,i);
                GWorld.addOneObject(new Water(),4,i);
            }
        }
        else if(dir == 1){
            for(int i=0;i<8;i++){
                GWorld.addOneObject(new Water(),2,i);
                GWorld.addOneObject(new Water(),5,i);
            }
        }
        else if(dir == 2){
            for(int i=0;i<8;i++){
                GWorld.addOneObject(new Water(),i%4,(i/4)*2+1);
                GWorld.addOneObject(new Water(),i%4,(i/4)*2+5);
            }
        }
        else {
            for(int i=0;i<8;i++){
                GWorld.addOneObject(new Water(),i%4,(i/4+1)*2-1);
                GWorld.addOneObject(new Water(),i%4,(i/4+1)*2+3);
            }
        }
        emptyDirtCell(dir);
    }    
    /**
     * add the caves,waters and traps.
     */
    private void initializeLandscape() {
        
        // Create the Landscape cells here...
        // Examples: 
        GWorld.addOneObject( new Cave(), 3, 0 );  // Cave for RED
        GWorld.addOneObject( new Cave(), 3, 8 );  // Cave for Green
        //add traps;
        int[] trap = {8,7,8};
        for(int i=0;i<3;i++){
            GWorld.addOneObject( new Trap(),i+2,trap[i]);
            GWorld.addOneObject( new Trap(),i+2,8-trap[i]);
        }
        //add water;
        for(int i=1;i<=5;i++)
        {
            if (i==3)
                continue;
            for(int n=3;n<=5;n++)
            {
                Water w = new Water();
                GWorld.addOneObject(w,i,n);
            }
        }
    }
    
    private static Actor[] alter;
     /**
     * save all the chess pieces.
     */
    public void save() {
        Actor[] allChess = GWorld.getAllObjects("Chess");
        if(allChess != null){
            alter = new Actor[allChess.length];
            for(int i=0;i<allChess.length;i++){
                alter[i] = allChess[i];
            }
        }
    }
    
    /**
     * check whether some has won;
     */
    public void checkWin(){
        String content;
        String r = "Red wins!";
        String g = "Green wins!";
        Chess redChess = (Chess)getOneObjectAt(3,8,"Chess");
        Chess greenChess = (Chess)getOneObjectAt(3,0,"Chess");
        if(redChess != null&&redChess.color==0){
            content = r;
        }
        else if(greenChess != null&&greenChess.color==1){
            content = g;
        }
        else{
            int redNum = 0;
            int greenNum = 0;
            java.util.List<Chess> chess = getObjects(Chess.class);
            for(Chess che : chess){
                if(che.color == 0){
                    redNum++;
                }
                else{
                    greenNum++;
                }
            }
            if(redNum == 0 && greenNum != 0){
                content = g;
            }
            else if(redNum != 0 && greenNum == 0){
                content = r;
            }
            else{
                content = "No one wins yet!";
            }
        }
        JOptionPane.showMessageDialog(new JInternalFrame(),content,"Message title", JOptionPane.INFORMATION_MESSAGE);
    }
            
    /**
     * load all animals and add caves,waters and traps again.
     */
    public void load(){
        if(alter!=null){
            //load landscape;
            initializeLandscape();
            //load animals;
            for(int i=0;i<alter.length;i++){
                GWorld.addOneObject(alter[i],alter[i].getX(),alter[i].getY());
            }
        }
    }
          
}
