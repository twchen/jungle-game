import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Chess here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Chess extends Actor
{
    public int color;
    public int rank;

    public static final int COLOR_RED = 0;
    public static final int COLOR_GREEN = 1;
    
    public static final int RANK_RAT = 1;
    public static final int RANK_CAT = 2;
    public static final int RANK_DOG = 3;
    public static final int RANK_WOLF = 4;
    public static final int RANK_LEOPARD = 5;
    public static final int RANK_TIGER = 6;
    public static final int RANK_LION = 7;
    public static final int RANK_ELEPHANT = 8;
    
    private Chess ch1,ch2;//ch1 is the current chess and ch2 is the chess beside ch1 in the specific direction,if any;
    private Water w1,w2;//w1(w2) is the water object(if any) at the location of ch1(ch2);
    private Trap tr;//tr is the trap object(if any) at the location of ch2;
    private int x1,y1,x2,y2;//(x1,y2) is the coordinate of ch1,and (x2,y2) is th coordinate of ch2;
    
    /**
     * return true if the chess ch1 can move towards the specific direction;
     * note that 0 is up,1 is left,2 is down,and 3 is right;
     */
    private boolean canMove(int dir){
        getChess(dir);
        w1 = (Water)GWorld.getOneObjectAt(x1,y1,"Water");
        w2 = (Water)GWorld.getOneObjectAt(x2,y2,"Water");
        if(w1 == null){
            if(ch2 == null){
                 if(w2 != null){
                        if(ch1.rank==1 || ch1.rank==3){
                            return true;
                        }
                        else return false;
                 }
                 else return true;
            }
            else {
                if(ch1.color == ch2.color){
                    return false;
                }
                else{
                    if(w2 != null){
                        if((ch1.rank==1||ch1.rank==3) && ch1.rank >= ch2.rank){
                            return true;
                        }
                        else return false;
                    }
                    else {
                        tr = (Trap)GWorld.getOneObjectAt(x2,y2,"Trap");
                        if(tr != null && ch1Trap() == true){
                            return true;
                        }
                        else return checkRank();
                    }
                }
            }
        }
        else{
            if(ch2 == null){
                return true;
            }
            else{
                if(ch1.color == ch2.color){
                    return false;
                }
                else {
                    if(w2 != null){
                        return checkRank();
                    }         
                    else return false;
                }
            }        
        }
    }
    
    /**
     * get the chess pieces ch1 and ch2,if any;
     */
    private void getChess(int dir){
        ch1 = this;
        x1 = ch1.getX();
        y1 = ch1.getY();
        switch(dir){
            case 0:
            case 2:x2 = x1;break;
            case 1:x2 = x1-1;break;
            case 3:x2 = x1+1;break;
        }
        switch(dir){
            case 1:
            case 3:y2 = y1;break;
            case 0:y2 = y1-1;break;
            case 2:y2 = y1+1;break;
        }
        ch2 = (Chess)GWorld.getOneObjectAt(x2,y2,"Chess");
    }
    
    /**
     * check whether ch2 is located at the ch1's trap;
     */
    private boolean ch1Trap(){
        int a = ch1.color*7;
        int b = tr.getY();
        if(a == b || a+1 == b){
            return true;
        }
        else return false;
    }
    
    /**
     * move one step towards the specific direction and remove ch2,if any;
     */
    private void move1(int dir){
        if(canMove(dir) == true){
            if(ch2 != null){
                sound(ch2.rank);
                GWorld.removeOneObject(ch2);
            }
            setLocation(x2,y2);
        }
    }
    
    /**
     * constructor of the Chess class;
     */
    public Chess(int c, int r)
    {
        color = c ;
        rank = r;
        updateImage();
    }
    
    /**
     * return true if the rank of ch1 if higher than that of ch2;
     */
    private boolean checkRank(){
        int rank1 = ch1.rank;
        int rank2 = ch2.rank;
        if(rank1==1){
            if(rank2==1 || rank2==8){
                return true;
            }
            else {
                return false;
            }
        }
        else if(rank1==8){
            if(rank2 != 1){
                return true;
            }
            else{
                return false;
            }
        }
        else if(rank1<8 && rank1>1){
            if(rank1>=rank2){
                return true;
            }
            else{
                return false;
            }
        }
        else return false;
    }
    
    
    /**
     * Implement the up() movement
     */
    public  void up()
    {
            move1(0);
    }
    
     /**
     * Implement the down() movement
     */
    public void down()
    {
            move1(2);
    }
    
    /**
     * Implement the left() movement
     */
    public void left()
    {
           move1(1);
    }
    
    /**
     * Implement the up() movement
     */
    public void right()
    {
           move1(3);
    }
    
    /**
     * play the scream of ch2 when it is killed;
     */
    public void sound(int rank){
        String[] sounds = {"rat","cat","dog","wolf","leopard","tiger","lion","elephant"};
        Greenfoot.playSound(sounds[rank-1]+".wav");
    }
    
    /**
     * It simply loads the image based on the given rank and color, No need to modify.
     */    
    private void updateImage()
    {
        String prefix = "";
        String postfix = "" ;        
        if ( color == COLOR_RED )
        {
            prefix = "red_";    
        }
        else
        {
            prefix = "green_";
        }
        switch (rank)
        {            
            case RANK_RAT: postfix = "rat"; break;
            case RANK_CAT: postfix = "cat"; break; 
            case RANK_DOG: postfix = "dog"; break;
            case RANK_WOLF: postfix = "wolf"; break;
            case RANK_LEOPARD: postfix = "leopard"; break;
            case RANK_TIGER: postfix = "tiger"; break;
            case RANK_LION: postfix = "lion"; break;
            default: 
            case RANK_ELEPHANT: postfix = "elephant"; break;

        }        
        setImage( prefix + postfix + ".png" );        
    }
    
        

    /**
     * Nothing happens
     */
    public void act() 
    {
        switch(TestWorld.movementDir){
            case 1:up();break;
            case 2:left();break;
            case 3:down();break;
            case 4:right();break;
        }
    }    // Add your action code here.
}
