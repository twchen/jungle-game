import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class JumpableChess here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class JumpableChess extends Chess
{
    private int x;//the x coordinate of the current chess;
    private int y;//the y coordinate of the current chess;
    public Chess ch2;//the chess at the place which the current chess is about to jump to;
    
    public JumpableChess(int c,int r){
        super(c,r);
    }
    
    /**
     * the jump up method;
     */
    public void jumpUp(){
        if(upOrDown(1)==true){
            ch2 = (Chess)GWorld.getOneObjectAt(x,8-y,"Chess");
            if(ch2!=null){
                if(this.color!=ch2.color){
                    if(checkRank()==true){
                        ch2.sound(ch2.rank);
                        GWorld.removeOneObject(ch2);
                        setLocation(x,2);
                    }
                }
            }
            else setLocation(x,2);
        }
    }
    
    /**
     * the jump down method;
     */
    public void jumpDown(){
        if(upOrDown(-1)==true){
            ch2 = (Chess)GWorld.getOneObjectAt(x,8-y,"Chess");
            if(ch2!=null){
                if(this.color!=ch2.color){
                    if(checkRank()==true){
                        ch2.sound(ch2.rank);
                        GWorld.removeOneObject(ch2);
                        setLocation(x,6);
                    }
                }
            }
            else setLocation(x,6);
        }
    }
    
    /**
     * the jump left method;
     */
    public void jumpLeft(){
        x = getX();
        y = getY();
        if(x != 0){
            if(leftOrRight(-1)==true){
                 ch2 = (Chess)GWorld.getOneObjectAt(x-3,y,"Chess");
                 if(ch2 != null){
                     if(this.color!=ch2.color){
                         if(checkRank()==true){
                             ch2.sound(ch2.rank);
                             GWorld.removeOneObject(ch2);
                             setLocation(x-3,y);
                         }
                     }
                 }
                 else setLocation(x-3,y);
            }
        }
    }
    
    /**
     * the jump right method;
     */
    public void jumpRight(){
        x = getX();
        y = getY();
        if(x != 6){
            if(leftOrRight(1)==true){
                 ch2 = (Chess)GWorld.getOneObjectAt(x+3,y,"Chess");
                 if(ch2 != null){
                     if(this.color!=ch2.color){
                         if(checkRank()==true){
                             ch2.sound(ch2.rank);
                             GWorld.removeOneObject(ch2);
                             setLocation(x+3,y);
                         }
                     }
                 }
                 else setLocation(x+3,y);
            }
        }
    }
    
    /**
     * check whether the current chess can jump up or jump down;
     */
    private boolean upOrDown(int a){
        x = getX();
        y = getY();
        if((x==1||x==2||x==4||x==5)&&(y==2||y==6)){
            Chess ch = null;
            ch = (Chess)GWorld.getOneObjectAt(x,8-(y-a),"Chess");
            if(ch != null){
                return false;
            }
            else{
                ch = (Chess)GWorld.getOneObjectAt(x,8-(y-2*a),"Chess");
                if(ch != null){
                    return false;
                }
                else{
                    ch = (Chess)GWorld.getOneObjectAt(x,8-(y-3*a),"Chess");
                    if(ch != null){
                        return false;
                    }
                    else{
                        return true;
                    }
                }
            }
        }
        else return false;
    }
    
    /**
     *  check whether the current chess can jump right or jump left;
     */
    private boolean leftOrRight(int b){
        if(y<6&&y>2&&(x==0||x==3||x==6)){
            Chess ch = null;
            ch = (Chess)GWorld.getOneObjectAt(x+b,y,"Chess");
            if(ch != null){
                return false;
            }
            else{
                ch = (Chess)GWorld.getOneObjectAt(x+2*b,y,"Chess"); 
                if(ch != null){
                    return false;
                }
                else{
                    return true;
                }
            }
        }
        else return false;
    }
    
    /**
     * compare the rank of the current chess and the other chess;
     */
    private boolean checkRank(){
        int rank1 = this.rank;
        int rank2 = ch2.rank;
        if(rank1==1){
            if(rank2==1 || rank2==8){
                return true;
            }
            else {
                return false;
            }
        }
        else if(rank1==8){
            if(rank2 != 1){
                return true;
            }
            else{
                return false;
            }
        }
        else if(rank1<8 && rank1>1){
            if(rank1>=rank2){
                return true;
            }
            else{
                return false;
            }
        }
        else return false;
    }
    
    /**
     * Act - do whatever the JumpableChess wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
}
